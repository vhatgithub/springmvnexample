package com.cognizant.sample;

public class HelloWorld {
	private  String greeting;
	
	public String getGreeting() {
		return greeting;
	}
	
	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
	public void greet() {
		System.out.println(this.greeting);
	}
}
